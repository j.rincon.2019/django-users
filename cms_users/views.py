from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Contenido
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout

def index(request):
    content_list = Contenido.objects.all()
    template = loader.get_template('cms_user_put/index.html')
    contexto = {
        'content_list': content_list,
    }
    return HttpResponse(template.render(contexto, request))


formulario = """
<form action="" method="POST">
"""


@csrf_exempt
def get_resource(request, recurso):
    if request.method in ["PUT", "POST"]:
        if request.method == "POST":
            valor = request.POST["valor"]
        else:
            valor = request.body.decode()

        try:
            contenido = Contenido.objects.get(clave=recurso)
            contenido.valor = valor
            contenido.save()
        except Contenido.DoesNotExist:
            save_content(request, recurso)

    try:
        contenido = Contenido.objects.get(clave=recurso)
        respuesta = HttpResponse(f"El recurso pedido es: {contenido.clave}. Su valor es: {contenido.valor}. Su identificador es: {contenido.id}")
    except Contenido.DoesNotExist:
        respuesta = handle_content_does_not_exist(request)

    return respuesta


@login_required
def handle_content_does_not_exist(request):
    return HttpResponse("Para poder dar de alta un recurso tienes que estar <a href=/login/>autenticado</a>")


def save_content(request, recurso):
    if request.method == "POST":
        valor = request.POST["valor"]
    else:
        valor = request.body.decode()

    try:
        contenido = Contenido.objects.get(clave=recurso)
    except Contenido.DoesNotExist:
        contenido = Contenido(clave=recurso)

    contenido.valor = valor
    contenido.save()

@login_required
def loggedIn(request):
    respuesta = f"Bienvenido {request.user.username} al CMS de SARO"
    return HttpResponse(respuesta)

def logout_vista(request):
    logout(request)
    return redirect("/login")

def imagen(request):
    template = loader.get_template("cms_user_put/plantilla.html")
    context = {}
    return HttpResponse(template.render(context, request))
