from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('acceso/', views.loggedIn),
	path('logout/', views.logout_vista),
	path('<str:recurso>/', views.get_resource),
]
